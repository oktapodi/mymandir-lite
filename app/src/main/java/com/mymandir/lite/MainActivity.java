package com.mymandir.lite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    private WebView mmMainWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupMainWebView();
        makeWebViewRequest();
    }

    private void setupMainWebView() {
        mmMainWebView = (WebView) findViewById(R.id.mm_main_web_view);
        mmMainWebView.setWebViewClient(new WebViewClient() {
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                return super.shouldInterceptRequest(view, request);
            }
        });
    }

    private void makeWebViewRequest() {
        mmMainWebView.loadUrl(getString(R.string.app_name));
    }
}
