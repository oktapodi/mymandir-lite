package com.mymandir.lite;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;

/**
 * Created by prabhusaitu on 24/01/18.
 */

public class MMWebViewJavaScriptInterface {

    Context mContext;

    public MMWebViewJavaScriptInterface(Context mContext) {
        this.mContext = mContext;
    }

    private boolean isURIAvailable(String uri) {
        Intent test = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        return mContext.getPackageManager().resolveActivity(test, 0) != null;
    }

    @JavascriptInterface
    public void addBooleanInSharedPreferences(String key, boolean value) {
        UtilityHelper.putInSharedPreference(mContext, key, value);
    }

    @JavascriptInterface
    public void addStringInSharedPreferences(String key, String value) {
        UtilityHelper.putInSharedPreference(mContext, key, value);
    }

    @JavascriptInterface
    public void addNumberInSharedPreferences(String key, int value) {
        UtilityHelper.putInSharedPreference(mContext, key, value);
    }

}
