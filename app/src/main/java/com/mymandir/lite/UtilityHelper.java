package com.mymandir.lite;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by prabhusaitu on 24/01/18.
 */

public class UtilityHelper {

    private static int windowWidth;
    private static int windowHeight;


    public static boolean isExternalStorageDocument(@NonNull Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(@NonNull Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(@NonNull Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    //SharedPreferences Helper Function//
    public static SharedPreferences getSharedPreferencesObject(@NonNull Context context) {
        return context.getSharedPreferences("MMPreferences", Context.MODE_PRIVATE);
    }

    public static long getLongFromSharedPreferences(@NonNull Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getLong(key, 0);
    }

    public static int getIntFromSharedPreferences(@NonNull Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getInt(key, 0);
    }

    @Nullable
    public static String getStringFromSharedPreference(@NonNull Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref.getString(key, "0");
    }

    public static boolean getBooleanFromSharedPreference(@NonNull Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences("MMPreferences", Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key, false);
    }

    @SuppressLint("ApplySharedPref")
    public static <T> void putInSharedPreference(@NonNull Context context, String key, T t) {
        Log.i("SP", "putting in sharedPref " + key);
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (t instanceof String) {
            editor.putString(key, (String) t);
        } else if (t instanceof Long) {
            editor.putLong(key, (Long) t);
        } else if (t instanceof Integer) {
            editor.putInt(key, (Integer) t);
        } else if (t instanceof Boolean) {
            editor.putBoolean(key, (Boolean) t);
        } else if (t instanceof Float) {
            editor.putFloat(key, (Float) t);
        }
        editor.commit();
    }


    public static int getScreenResolution(@NonNull Context context, @NonNull String widthOrHeight) {
        if (windowHeight == 0 && windowWidth == 0) {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            windowWidth = metrics.widthPixels;
            windowHeight = metrics.heightPixels;
        }

        if (widthOrHeight.equals("width")) {
            return windowWidth;
        }
        return windowHeight;
    }

    public static int dpToPxOriginal(@NonNull Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    public static int dpToPx(@NonNull Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(@NonNull Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

}
